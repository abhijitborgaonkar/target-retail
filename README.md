System Requirement
1. target-retail-service and target-auth-service project requires java 1.8 and Maven
2. target-product-catalogue project requires node 1.8

To verify if java is installed : Type "java --version" on command prompt

To verify if node is installed : Type "node --version" on command prompt 

Download the code base.
====================================================================
1. Open command prompt
2. Navigate to folder where you want to download the code (referred as SRC_FOLDER henceforth)
2. Type following command. This will download the project from github
	git clone https://gitlab.com/abhijitborgaonkar/target-retail.git


About Projects
====================================================================
The code base has 3 projects

1. target-retail-service : 
This project provides various end points(webservices) related to item like get item, add/update item etc.


2. target-product-catalouge
This is a angular project which provides UI screens for viewing, adding and updating the items

3. target-auth-service
This is a microservice which provides authentication/authorization services.
Work in progress for auth.


Build 
====================================================================
1. Build target-retail-service jar
	a. Open command prompt
	b. Navigate to SRC_FOLDER/target-retail-service folder.
	c. Type following command and hit enter.  
			maven clean install
	d. This command will generate "target-retail-service-0.0.1-SNAPSHOT.jar" in "SRC_FOLDER/target-retail-service/target" folder

2. Build target-auth-service jar
	a. Open command prompt
	b. Navigate to SRC_FOLDER/target-auth-service folder.
	c. Type following command and hit enter.  
			maven clean install
	d. This command will generate "target-auth-service-0.0.1-SNAPSHOT.jar" in "SRC_FOLDER/target-auth-service/target" folder

	
	
Deploy or Run
====================================================================
1. target-retail-service 
	a. open command prompt and navigate to "SRC_FOLDER/target-retail-service/target" folder where "target-retail-service-0.0.1-SNAPSHOT.jar" is available
	b. Type following command on command prompt
		java -jar target-retail-service-0.0.1-SNAPSHOT.jar
	c. This will start the application 
	d. To access the application, open browser and type following address
		localhost:8080/item/get/{1213}
		This should give a json response with error "Item Id does not exists in Db"
		
2. target-product-catalouge : Angular project
	a. Open command prompt and navigate to SRC_FOLDER/target-product-catalouge
	d. Type following command. This will start the Angular application.
		ng serve --open
	e. Application can be accessed using url http://localhost:2400   


3. target-retail-service 
	a. open command prompt and navigate to "SRC_FOLDER/target-auth-service/target" folder where "target-auth-service-0.0.1-SNAPSHOT.jar" is available
	b. Type following command on command prompt
		java -jar target-auth-service-0.0.1-SNAPSHOT.jar
	c. This will start the application




