package com.target.service;

import java.util.List;

import com.target.domain.Item;
import com.target.exception.DataNotFoundException;

public interface ItemService {

	Item addItem(Item item);
	
	Item getItem(String itemId) throws DataNotFoundException;
	
	
	void removeItem(String itemId);

	List<Item> getItems(int offset, int limit) throws DataNotFoundException;

}
