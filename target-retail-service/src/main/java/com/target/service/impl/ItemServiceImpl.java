package com.target.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.target.controller.ItemController;
import com.target.domain.Item;
import com.target.exception.DataNotFoundException;
import com.target.repository.ItemRepository;
import com.target.service.ItemService;
@Service
public class ItemServiceImpl implements ItemService {

	private static final Logger LOGGER = LogManager.getLogger(ItemServiceImpl.class);
	
	@Autowired
	private ItemRepository itemRepository;

	@Override
	public Item addItem(Item item) {
		return itemRepository.save(item);
	}

	@Override
	public Item getItem(String itemId) throws DataNotFoundException {
		Optional<Item> optionalItem = itemRepository.findById(itemId);
		if (optionalItem.isPresent()) {
			LOGGER.debug("Item exist in DB");
			return optionalItem.get();
		} else {
			throw new DataNotFoundException("Item Id does not exists in Db");
		}

	}

	@Override
	public void removeItem(String itemId) {
		itemRepository.deleteById(itemId);
		LOGGER.debug("Item deleted successfully");
	}

	@Override
	public List<Item> getItems(int offset, int limit) throws DataNotFoundException {
		Pageable page = PageRequest.of(offset, limit);
		Page<Item> res = itemRepository.findAll(page);
		if (res.isEmpty()) {
			throw new DataNotFoundException("Unable to fetch the items. Possible reason : offset could be wrong");
		} else {
			return res.getContent();
		}
	}

}
