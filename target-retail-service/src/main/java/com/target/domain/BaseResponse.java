package com.target.domain;

import java.util.List;

public class BaseResponse<T> {

	private String responseCode;
	private String responseMessage;
	private T payload;
	private List<ErrorDetails> errorDetails;
	
	
	public List<ErrorDetails> getErrorDetails() {
		return errorDetails;
	}
	public void setErrorDetails(List<ErrorDetails> errorDetails) {
		this.errorDetails = errorDetails;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public T getPayload() {
		return payload;
	}
	public void setPayload(T payload) {
		this.payload = payload;
	}
}
