package com.target.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="products")
public class Item {

	@Id
	private String itemId;
	private String itemName;
	private String itemDescription;
	private double itemDimensionLength;
	private double itemDimensionWidth;
	private double itemDimensionHeigth;
	private double itemWeight;
	private double itemPrice;
	
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public double getItemDimensionLength() {
		return itemDimensionLength;
	}
	public void setItemDimensionLength(double itemDimensionLength) {
		this.itemDimensionLength = itemDimensionLength;
	}
	public double getItemDimensionWidth() {
		return itemDimensionWidth;
	}
	public void setItemDimensionWidth(double itemDimensionWidth) {
		this.itemDimensionWidth = itemDimensionWidth;
	}
	public double getItemDimensionHeigth() {
		return itemDimensionHeigth;
	}
	public void setItemDimensionHeigth(double itemDimensionHeigth) {
		this.itemDimensionHeigth = itemDimensionHeigth;
	}
	public double getItemWeight() {
		return itemWeight;
	}
	public void setItemWeight(double itemWeight) {
		this.itemWeight = itemWeight;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", itemName=" + itemName + ", itemDescription=" + itemDescription
				+ ", itemDimensionLength=" + itemDimensionLength + ", itemDimensionWidth=" + itemDimensionWidth
				+ ", itemDimensionHeigth=" + itemDimensionHeigth + ", itemWeight=" + itemWeight + ", itemPrice="
				+ itemPrice + "]";
	}
	
	
	
}
