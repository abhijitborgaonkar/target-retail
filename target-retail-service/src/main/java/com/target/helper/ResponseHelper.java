package com.target.helper;

import com.target.domain.BaseResponse;

public class ResponseHelper {

	public static  <T> BaseResponse<T> createResponse(String responseCode,String responseMessage, T payload){
		BaseResponse<T> response=new BaseResponse<>();
		response.setPayload(payload);
		response.setResponseCode(responseCode);
		response.setResponseMessage(responseMessage);
		return response;
	}
}
