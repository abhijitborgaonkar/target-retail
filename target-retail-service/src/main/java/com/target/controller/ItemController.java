package com.target.controller;

import java.util.List;

import javax.validation.constraints.Max;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.target.constant.RetailConstants;
import com.target.domain.Item;
import com.target.exception.DataNotFoundException;
import com.target.service.ItemService;

@RestController
@CrossOrigin(origins = "http://localhost:4200") // TODO: NEED TO MAKE THIS GLOBAL CONFIG
@RequestMapping("/item")
public class ItemController {

	private static final Logger LOGGER = LogManager.getLogger(ItemController.class);

	@Autowired
	private ItemService itemService;

	/**
	 * This method adds/updates the item to database
	 * 
	 * @param item
	 * @return
	 */
	@PostMapping({ "/add", "/update" })
	public ResponseEntity<Item> addOrUpdateItem(@RequestBody Item item) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("addOrUpdateItem - {}", item);
		}
		Item savedItem = itemService.addItem(item);
		return ResponseEntity.status(HttpStatus.OK).body(savedItem);
	}

	/**
	 * Method deletes item from database
	 * @param itemId
	 * @return
	 */
	@DeleteMapping("/delete/{itemId}")
	public ResponseEntity<String> deleteItem(@PathVariable("itemId") String itemId) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("removeItem with id - {}", itemId);
		}
		itemService.removeItem(itemId);
		return ResponseEntity.status(HttpStatus.OK).body(RetailConstants.ITEM_REMOVED_SUCCESSFULLY);
	}

	/**
	 * Method fetches the Item object from Database for the given item id.
	 * If item not found in DB, method returns 401-not fount status code error.
	 * @param itemId
	 * @return
	 * @throws DataNotFoundException 
	 */
	@GetMapping("/get/{itemId}")
	public ResponseEntity<Object> getItemById(@PathVariable("itemId") String itemId) throws DataNotFoundException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getItemById - {}", itemId);
		}
		Item item = itemService.getItem(itemId);
		return ResponseEntity.status(HttpStatus.OK).body(item);
		
	}

	/**
	 * Method fetches the items from Db. The number of items is limited by the imput parameter 'limit'.
	 * The offset parameter determines which page to fetch from Db.
	 * @param offset
	 * @param limit
	 * @return
	 * @throws DataNotFoundException 
	 */
	@GetMapping("/items")
	public ResponseEntity<List<Item>> getItems(@RequestParam("offset") int offset,
			@Max(value = 25) @RequestParam("limit") int limit) throws DataNotFoundException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getItems invoked. offset - {},limit-{}", offset, limit);
		}
		List<Item> items = itemService.getItems(offset, limit);
		return ResponseEntity.status(HttpStatus.OK).body(items);
	}

	// Search Methods

}
