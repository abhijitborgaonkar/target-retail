package com.target.advice;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.target.domain.ErrorDetails;
import com.target.exception.DataNotFoundException;

/**
 * Exception Handler class to handle all the exceptions from Endpoints. 
 * @author 
 *
 */
@ControllerAdvice
@RestController
public class TargetServiceExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LogManager.getLogger(TargetServiceExceptionHandler.class);
	
	@ExceptionHandler(DataNotFoundException.class)
	public final ResponseEntity<ErrorDetails> handleDataNotFoundException(DataNotFoundException ex,
			WebRequest request) {
		LOGGER.debug("handleDataNotFoundException invoked");
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
}
