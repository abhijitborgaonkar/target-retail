package com.target.constant;

public class RetailConstants {

	public static final String RESPONSE_CODE_SUCCESSFUL="001";
	public static final String RESPONSE_CODE_FAILURE="002";

	public static final String ITEM_REMOVED_SUCCESSFULLY = "Item removed Successfully";
	
	public static final String ITEM_SAVED_MESSAGE="Item Saved Successfully";
	public static final String ITEM_FOUND_MESSAGE = "Item Found";
	public static final String ITEMS_FETCHED_MESSAGE = "Items Fetched from Database";
	public static final String ITEMS_REMOVED_MESSAGE = "Item removed successfully";
}
