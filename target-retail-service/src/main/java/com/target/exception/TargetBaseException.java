package com.target.exception;

public class TargetBaseException extends Exception{

	public TargetBaseException() {
		super();
	}
	
	public TargetBaseException(String errorMessage) {
		super(errorMessage);
	}
	
	public TargetBaseException(String errorMessage,Throwable t) {
		super(errorMessage,t);
	}
}
