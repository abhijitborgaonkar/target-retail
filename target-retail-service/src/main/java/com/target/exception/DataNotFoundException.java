package com.target.exception;

public class DataNotFoundException extends TargetBaseException{

	public DataNotFoundException() {
		super();
	}

	public DataNotFoundException(String errorMessage, Throwable t) {
		super(errorMessage, t);
	}

	public DataNotFoundException(String errorMessage) {
		super(errorMessage);
	}

}
