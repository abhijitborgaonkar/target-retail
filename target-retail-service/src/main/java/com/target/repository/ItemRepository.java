package com.target.repository;



import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.target.domain.Item;

@Repository
public interface  ItemRepository extends MongoRepository<Item, String>{

	Item findByItemName(String itemName);
}
