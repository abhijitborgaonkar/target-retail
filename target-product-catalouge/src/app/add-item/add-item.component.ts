import { Component, OnInit } from '@angular/core';
import { Item } from '../item';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { CookieService } from "angular2-cookie/core";
import { GwtService } from '../gwt.service';


@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})


export class AddItemComponent implements OnInit {

  model = new  Item();
  constructor(private http: HttpClient,private cookieService: CookieService,private service:GwtService) { 
	   this.model = new  Item();
	   this.submitted = false;
  }

  ngOnInit() {
  }

  newItem(){
   this.model = new  Item();
   this.submitted = false;
  }

  submitted = false;
  submitMessage ="";
  onSubmit() { 
	this.submitted = true; 
	let serializedForm = JSON.stringify(this.model);
	console.log("serializedForm---!", serializedForm);
	let token=this.cookieService.get('access_token');
	const httpOptions = {
		  headers: new HttpHeaders({
			    'Content-Type':'application/json',
			    'Accept':  'application/json',
			    'Authorization': 'Bearer '+ token
			})
		};
        this.http.post("http://localhost:8080/item/add", serializedForm, httpOptions)
	.subscribe(
	        (val) => {
		    this.submitMessage="Item Saved Successfully";
		     //this.model.itemId=val.itemId;
	        },
	        response => {
		    this.submitMessage="Save Failed. Try again";
	            console.log("POST call in error", response);
	        },
	        () => {
	            console.log("The POST observable is now completed.");
		   }
	);
	console.log("After http call");

  }
logout():void{
		this.service.logout();
	}

}
