import { TestBed } from '@angular/core/testing';

import { GwtService } from './gwt.service';

describe('GwtService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GwtService = TestBed.get(GwtService);
    expect(service).toBeTruthy();
  });
});
