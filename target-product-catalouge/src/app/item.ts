export class Item {
	itemId: string;
	itemName: string;
	itemDescription: string;
	itemDimensionLength: number;
	itemDimensionWidth: number;
	itemDimensionHeigth: number;
	itemWeight: number;
	itemPrice: number;
 
}
