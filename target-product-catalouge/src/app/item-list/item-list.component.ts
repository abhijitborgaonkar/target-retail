import { Component, OnInit } from '@angular/core';
import { Item } from '../item';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { CookieService } from "angular2-cookie/core";
import { GwtService } from '../gwt.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

	items : Object;

	constructor(private http: HttpClient,private cookieService: CookieService,private service:GwtService) { 
		this.getItems();
	}

	ngOnInit() {
		this.getItems();
	}

	selectedItem: Item;
	
	onSelect(item: Item): void {
	  this.selectedItem=item;
	}

	logout():void{
		this.service.logout();
	}
	getItems(): void {
	let token=this.cookieService.get('access_token');
	const httpOptions = {
		  headers: new HttpHeaders({
			    'Accept':  'application/json',
			    'Authorization': 'Bearer '+ token
			})
		};
		this.http.get("http://localhost:8080/item/items?offset=0&limit=100",httpOptions)
	.subscribe(
	        (response) => {
	            console.log("get call successful value returned in body", response);
		    this.items=response;			
	        },
	        response => {
	            console.log("get call in error", response);
	        },
	        () => {
	            console.log("The get observable is now completed.");
		}
	);
	}

}
