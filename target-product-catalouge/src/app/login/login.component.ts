import { Component, OnInit } from '@angular/core';
import { GwtService } from '../gwt.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginData = {username: "", password: ""};


  constructor(private service:GwtService) { }

  ngOnInit() {
  }

   login() {
        this.service.obtainAccessToken(this.loginData.username,this.loginData.password);
    }

}
