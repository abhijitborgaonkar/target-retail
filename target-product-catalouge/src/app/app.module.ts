import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { AddItemComponent } from './add-item/add-item.component';
import { RouterModule, Routes } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ItemListComponent } from './item-list/item-list.component';
import { LoginComponent } from './login/login.component';
import { HomepageComponent } from './homepage/homepage.component';
import { CookieService } from "angular2-cookie/services/cookies.service";

const appRoutes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'add-item', component: AddItemComponent },
	{ path: 'item-list', component: ItemListComponent },		
	{ path: '', redirectTo: '/login',    pathMatch: 'full'}
];
@NgModule({
  declarations: [
    AppComponent,
    ItemDetailComponent,
    AddItemComponent,
    ItemListComponent,
    LoginComponent,
    HomepageComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
