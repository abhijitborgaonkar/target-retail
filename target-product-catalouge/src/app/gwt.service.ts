import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { CookieService } from "angular2-cookie/core";
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GwtService {

	constructor(private httpClient: HttpClient,private cookieService: CookieService,private router: Router) { }



	obtainAccessToken(userName:string, password:string){
		let params = new URLSearchParams();
		params.append('username',userName);
		params.append('password',password);    
		params.append('grant_type','password');
		params.append('scope','webclient');
	    
		const httpOptions = {
		  headers: new HttpHeaders({
			'Content-type' : 'application/x-www-form-urlencoded; charset=utf-8',
		        'Authorization': 'Basic '+btoa("target-product-catalogue:thisissecret")
			})
		};
		this.httpClient.post('http://localhost:8081/auth/oauth/token',params.toString(), httpOptions)
	        .subscribe(
		        (response) => {	
			   this.saveToken(response);
			    console.log("POST call successful", response);
		        },
		        error => {
			   alert("Login failed "+error.error.error_description);
			    console.log("POST call in error", error);
		        },
		        () => {
		            console.log("The POST observable is now completed.");
		         }
		);
	 }
	 
	  saveToken(token){
	    var expireDate = new Date().getTime() + (1000 * token.expires_in);
	    this.cookieService.put("access_token", token.access_token);
	    this.router.navigate(['/item-list']);
	  }

	checkUserLogin(){
//	    if (!this.cookieService.check('access_token')){
		this.router.navigate(['/login']);
//	    }
	  } 
	 
	  logout() {
	    this.cookieService.remove('access_token');
	    this.router.navigate(['/login']);
	  }
		 
	  
}
